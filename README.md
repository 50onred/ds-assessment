# Data Science Assessment

This [glossary of Ad Tech terms]( https://wiki.appnexus.com/display/industry/Online+Advertising+and+Ad+Tech+Glossary) may come in handy to clarify some industry specific jargon.

#### Assessment Scenario

We are looking to develop a new product and want to find advertisers who we can pitch the idea to. The goal is to partner with these advertisers so we can work together and test the viability of the product idea.

The new product is meant to help prevent advertisers from exceeding a self specified CPA goal. CPA stands for Cost Per Acquisition and is calculated as `Total Costs / Total Acquisitions`. However in industry, the terms 'acquisition' and 'conversion' are often used interchangeably.

A conversion is an event that is self reported by an advertiser and the meaning of a given conversion can be industry specific or even advertiser specific. Frequently the meaning of a conversion is hidden from us.

Allowing advertisers to supply their own CPA goals would give them a useful way to adjust their spending practices based on the value they receive from their particular conversions.


#### The Data
This repo contains data that can be used to find advertisers who would likely benefit from a trial or alpha test of our CPA based product idea.

There are three files in the data folder:

*  advertisers.csv
    *  advertiser_id - numeric id of the advertiser.
    *  cpa_goal - This is the CPA goal we would have to meet before we'd feel comfortable approaching the advertiser for a testing partnership.


*  campaigns.csv
    *  campaign_id - numeric id of the campaign.
    *  advertiser_id - numeric id of the advertiser.
    *  bid_amount - the amount of money that the advertiser bids (and pays) if they win an impression.


*  impression_data.csv
    *  advertiser_id - numeric id of the advertiser.
    *  campaign_id - numeric id of the campaign.
    *  timestamp - time of when the impression was won.
    *  bid_floor - the minimum bid that the publisher would accept.
    *  browser - browser of the user.
    *  country - country of the user.
    *  creative_type - the type of ad that the user was shown.
    *  device_make - device_make of the user.
    *  device_model - device_model of the user.
    *  device_type - device_type of the user.
    *  isp - isp of the user.
    *  native_ad_unit - rtb related field.
    *  native_layout - rtb related field.
    *  publisher_id - numeric id of the publisher.
    *  site_url_domain - site the user was on when they were shown the ad.
    *  source_id - numeric id of the source.
    *  target_type - the type of target being sought by the advertiser.
    *  target_value - the value of the targeting match.
    *  user_clicks - number of times the user has clicked on any ad.
    *  user_ctr - the users historic click rate.
    *  user_impressions - number of times the user has been shown any ad.
    *  conversion - indicator variable for if the advertiser reported a conversion resulting from this ad impression.


#### The Ask

1. Install [git-lfs](https://git-lfs.github.com/).

    * The csv files in this repo are managed by git-lfs.


2. Clone this repo. Note that the impression_data.csv file is large (~900MB) so it may take a few minutes to download.

3. Use any means at your disposal to analyze the data in this repository to find advertisers who would likely benefit from a trial or alpha test of our new product idea.

    *  The product will not be a fit for all advertisers - in fact it will not be a fit for most advertisers in this data. However, there may be some advertisers whose data suggests that they could benefit from spending practices that take CPA into account.


#### The Deliverable

A list of any advertisers you think we should reach out to and any analysis output you used to arrive at this recommendation. A short explanation of how the output lead you to your conclusions would be very helpful in following your thought process.

Additionally, we would like to see the code you used in this assessment - as we are interested in both the analytical and technical skills required to complete this task.
