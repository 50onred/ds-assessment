"""
Usage:
  $ python run_classifier.py
"""
from __future__ import division
from pdb import set_trace as debug
import json
import pandas as pd
import numpy as np 
import pandas as pd
import logging 
import argparse 
from collections import OrderedDict
from pysqldf import SQLDF
from sklearn.cross_validation import train_test_split, KFold
from sklearn import metrics
from classifier_models import RFClassifier, GBClassifier
from evaluation_utils import train_test, cross_validate
from data_processing_utils import getLogger

logger = getLogger()
sqldf = SQLDF(globals())

##############################
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--classifier", default="rf")
    args = parser.parse_args()

    test_size = 0.3
    cv_folds = 3

    logger.info("Reading data... this may take some time")
    # Load data
    ad_df = pd.read_csv("advertisers.csv")
    campaigns_df = pd.read_csv("campaigns.csv")
    impression_df = pd.read_csv("impression_data.csv")

    #  Join tables 
    #  NB: There are no additional data processing, i.e., 
    # cleaning/imputing missing data in the interest of getting code operational.   
    ad_campaigns_df = \
        pd.merge(ad_df, campaigns_df, on='advertiser_id', how='left')
    df = \
        pd.merge(impression_df, ad_campaigns_df, on=['advertiser_id','campaign_id'], how='left')
    
    # Aggregate data
    q = "SELECT * FROM ( \
            SELECT d.advertiser_id, \
                  d.campaign_id, \
                  SUM(bid_amount) as total_cost, \
                  SUM(conversion) as total_acquisition, \
                  (SUM(bid_amount)/SUM(conversion)) as cpa, \
                  cpa_goal, \
                  (SUM(bid_amount)/SUM(conversion))/cpa_goal as cpa_over_goal_by, \
                  CASE WHEN (SUM(bid_amount)/SUM(conversion)) <= cpa_goal \
                  THEN 'no' \
                  ELSE 'yes' \
                  END as exceed_cpa_goal \
            FROM df d \
            GROUP BY d.advertiser_id, \
            d.campaign_id \
            HAVING SUM(bid_amount) < 700000 \
            ORDER BY (SUM(bid_amount)/SUM(conversion))/cpa_goal desc) \
            WHERE cpa_over_goal_by > 16;"

    #print sqldf.execute(q)
    logger.info("The advertisers to partner with are: {}".format(sqldf.execute(q)))

    # Drop all the columns with NANs
    # This is not the ideal way to handle dirty data
    # TODO Joseph to return to this after script is operationalized.
    df = df.dropna()

    # Get the number of folds fro cross validation
    kfold_n = len(df.index)

    models = {
        "rf": RFClassifier(label_col="conversion", feature_cols=["user_ctr", "user_impressions"],   
                           model_params={"n_jobs": 2, "random_state": 0}),}

    df_train, df_test = train_test_split(df, test_size=test_size, random_state=0)
    result = train_test(df_train, df_test, models[args.classifier])
    
    logger.info("train_test result is:\n%s\n" % json.dumps(result, indent=4, sort_keys=True))
    # performance = cross_validate(df, models[args.classifier], kfold_n, cv_folds=cv_folds)
    # logger.info("cross_validate result is:\n%s\n" %performance.to_string(line_width=144))

    logger.info("ALL DONE!\n")





