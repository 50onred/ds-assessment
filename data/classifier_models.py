"""
Suite of classifiers to aid model development.
"""
from __future__ import division
from pdb import set_trace as debug
import numpy as np
import pandas as pd

from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from data_processing_utils import getLogger

logger = getLogger()

class BaseClassifier(object):
    def __init__(self, label_col, feature_cols):
        self._label_col = label_col
        self._feature_cols = feature_cols
        self._model = None
    
    def get_label_col(self):
        return self._label_col
    
    def get_feature_cols(self):
        return self._feature_cols
    
    def fit(self, data):
        y, X = data[self._label_col], data[self._feature_cols]
        self._model.fit(X, y)
    
    def predict(self, data):
        X = data[self._feature_cols]
        y_pred = self._model.predict(X)
        return y_pred

    def predict_proba(self, data):
        """Predict probability (numerical values)
        """
        X = data[self._feature_cols]
        y_pred = self._model.predict_proba(X)
        return y_pred

    #NB: not all classifiers have feature importance
    def calc_feature_importance(self, data, verbose=True):
        """Calculate feature importance
        """
        self.fit(data)
        
        df = pd.DataFrame(zip(self._feature_cols,
            np.transpose(self._model.feature_importances_)))
        df.columns = ["feature", "importance_score"]
        df.sort_values(by="importance_score", axis=0, ascending=False, inplace=True)
        df.reset_index(drop=True, inplace=True)
        if verbose:
            logger.info("Sorted df is:\n%s" % df.to_string(line_width=96))
        return df


class RFClassifier(BaseClassifier):
    def __init__(self, label_col, feature_cols, model_params):
        super(RFClassifier, self).__init__(label_col, feature_cols)
        self._model = RandomForestClassifier(n_jobs=model_params["n_jobs"],
                                             random_state=model_params["random_state"])
  
class GBClassifier(BaseClassifier):
    def __init__(self, label_col, feature_cols, model_params):
        super(GBClassifier, self).__init__(label_col, feature_cols)
        self._model = GradientBoostingClassifier(learning_rate=model_params["learning_rate"],
                                n_estimators=model_params["n_estimators"],
                                random_state=model_params["random_state"])

