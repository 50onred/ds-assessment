---
title: "RED SPARK DATA SCIENCE ASSESSMENT"
output: pdf_document
---

Red Spark Data Analysis & Model
=====================================================================
# Synopsis
Red Spark is looking to launch a new product aimed at helping advertisers from exceeding self reported Cost Per Acquisiton (CPA) goals. 

In this report, I'll analyse Red Spark impression dataset to identify potential advertising partners to test the viability of the new product idea. I'll use heuristics along with a classificaion model to glean as much insight from the dataset. 

So first, let's begin with the end in mind:

1. What is the purpose of this assessment? **Data Science take home assignment to assess a candidate competency with predictive modeling/data analysis as part of the interview process.**

Second, now let's start at the beginning:

2. What's the problem to be solved? **The assignment asks to explore the dataset and list any advertisers that Red Spark should reach out to.**

# Data Processing

### Load the required packages
```{r message=FALSE}
suppressPackageStartupMessages(suppressWarnings({
  library(sqldf) 
  library(ggplot2)
  library(plyr)
  library(randomForest)
  library(rpart)
}))
```

Read the files directly from the working directory via: 

```{r results='hide'}
cmpn <- read.table("campaigns.csv", header=TRUE, sep = ",")
ad <- read.table("advertisers.csv", header=TRUE, sep = ",")
imp <- read.csv("impression_data.csv", header=TRUE, sep = ",")
```

In the interest of time and brevity, I won't display the contents of the individual dataframes or analyse them. Instead, I will aggregate them and focus on the output. In order to do so, I'll pull out R's sqlfd package: 

```{r, results=FALSE}
cmpn_ad<- sqldf("
          SELECT 
             c.campaign_id as cmpn_id,
             c.advertiser_id ad_id, 
             c.bid_amount, 
             a.cpa_goal 
             FROM cmpn c
                  JOIN ad a ON c.advertiser_id = a.advertiser_id ")

imp_df <- sqldf("
          SELECT * 
                FROM imp i
                LEFT JOIN cmpn_ad cad ON i.advertiser_id = cad.ad_id 
                                  AND i.campaign_id = cad.cmpn_id ")

df <- sqldf("
         SELECT d.advertiser_id, 
                  d.campaign_id, 
                  SUM(bid_amount) as total_cost, 
                  SUM(conversion) as total_acquisition, 
                  (SUM(bid_amount)/SUM(conversion)) as cpa, 
                  cpa_goal, 
                  (SUM(bid_amount)/SUM(conversion))/cpa_goal as cpa_over_goal_by,
                  CASE WHEN (SUM(bid_amount)/SUM(conversion)) <= cpa_goal 
                  THEN 'no' 
                  ELSE 'yes' 
                  END as exceed_cpa_goal 
            FROM imp_df d 
            GROUP BY d.advertiser_id, 
            d.campaign_id
            HAVING SUM(bid_amount) < 700000
            ORDER BY (SUM(bid_amount)/SUM(conversion))/cpa_goal desc")
```

### Now that we have our data aggregated. We should do some data munging on the data... However, before we do so, let's take a look at the structure.

```{r}
str(df)
```

Looking at the structure of the data, we see that the variable advertiser_id and campaign_id are integers. Let's convert them to the appropriate data type. I'll use character data type instead of integer.

```{r}
df$advertiser_id <- as.character(df$advertiser_id)
df$campaign_id <- as.character(df$campaign_id)
```

Now, let’s inspect the data to look for any weird behavior/wrong data. Data is never perfect and requires a lot of cleaning.

# Data Exploration
R summary function is usually the best place to start:

```{r}
summary(df)
```

###A few quick observations:

1. There are 22 unique advertisers and 67 uniques campaigns. Note: this is not displayed above but I gleaned it from an earlier look at the data. 
2. The median cpa goal is 50 while the actual median cpa that advertisers spend per campaign in this dataset is 236. 
3. There are several other variables with N/As or missing data especially in the impression dataset. I won't focus too much of those at the moment, but usually I would impute those missing values and try to analyse whether or not those missing values provide any additional insight for modeling or tell us about our problem space.

#Data Exploration

Now, let’s quickly investigate the aggregated dataset to see how some of the variables and their distribution differ. This will help us understand whether there is any information in our data in the first place and get a sense of the data.

##I never start by blindly building a machine learning/predictive model.

I usually **form a working hypothesis** and then **get a sense of the data.** Let’s just pick a couple of variables of interest to explore ... 

```{r}
ggplot(df, aes(total_acquisition, total_cost, label = df$advertiser_id)) + 
    geom_point(aes(colour=advertiser_id)) + 
  ggtitle("Total Cost vs. Total Acquisition")
```

As expected, the correlation between total cost and total acquisition is postive. There is quite some variation so I expect the R sqaure to be low. What is interesting is that we can quickly see the outliers, especially the advertisers who are spending a lot but not converting any customers. Note: keep in mind that conversion is a self reported metric by the advertisers and mean different things so we could be looking at some bias that is not immediately evident. 

```{r}
ggplot(df, aes(total_acquisition, total_cost, label = df$advertiser_id)) + 
  geom_label()
  ggtitle("Total Cost vs. Total Acquisition")
```

The plot above shows the advertiser ID as labels on the plot in order to better identify them.

```{r}
quantile(df$cpa_over_goal_by, na.rm = TRUE)
```

In this dataset, the top 25% of advertisers spend more than 15 times their specified cpa goal. Let's take a look at these advertisers since they would benefit the most from adjusting their spending practices based on the value they receive from their particular conversions.


```{r}
sqldf(" SELECT * 
          FROM df
             WHERE cpa_over_goal_by > 16 ")
```

I'd recommend pitching the idea to the following advertisers for the following campaign:

1. **10 61**

2. **20 60**

3. **21 55**

4. **2 58**

5. **16 2**

Advertiser_id 20 seems to be exceeding cpa goal on several campaigns so I would prioritizethem for a trial of the new product. 

While we don't know the value that these advertisers are getting from the customers they do acquire. It's clear that their spending practices have room for optimization.

# Modeling

Let’s now build a model to help us understand what drives a user to convert on an impression. We're not so much interested in building a model for prediction purposes at the moment but more so to gain any insight into features that may standout on what drives a user to convert. This will aid in how to optimize bid for advertisers per cpa goal. 

1. I 'll use Random Forest as the model with conversion as the target for classifier to gain insight. 

**I've selected random forest because:**

1. It usually requires very little time to optimize it (its default params are often close to the best ones) 
2. It is strong with outliers, irrelevant variables, continuous and discrete variables. 

```{r}
drops <- c("advertiser_id","campaign_id","timestamp", "publisher_id", "year", 
           "site_url_domain","source_id", "target_value","cmpn_id", 
           "ad_id","isp","device_model", "device_make")

model_df <- imp_df[ , !(names(imp_df) %in% drops)]
model_df <- as.data.frame(unclass(model_df))
model_df$conversion <- as.factor(model_df$conversion)
model_df <- model_df[complete.cases(model_df), ] # NB this is an extreme approach but in the interest of time I will adopt it to operationalize a model that can provide some insights. 
```

I'll create test/train set with 66% split and then build the forest with standard values for the 3 most important parameters (100 trees, trees as large as possible, 3 random variables selected at each split).

```{r}
train_sample = sample(nrow(model_df), size = nrow(model_df)*0.66)
train_data = model_df[train_sample,]
test_data = model_df[-train_sample,]
rf = randomForest(y=train_data$conversion, x = train_data[ , 1: (ncol(train_data)-3)],
                  ytest = test_data$conversion, xtest = test_data[ , 1: (ncol(train_data)-3)],
                  ntree = 100, mtry = 3, keep.forest = TRUE, classwt = c(0.7,0.3))

```

```{r}
rf
```

**Note: the numbers below may change slightly as I rerun the notebook.**

So, OOB error and test error are similar: 2.64% and 2.46% respectively. The dataset is extremely imbalanced with `7 in every 1000 impression resulting in a conversion`. Not surpising, the model does a good job of detecting users who will not convert, the true negative rate is 98%. There is some room for improvement with the true positive rate which capture the number of users that the model predicted did convert among all the users that actually converted. If I had more time and clearer business objectives, I could go for the best possible model that optmizes the business objective. Specifically, if we wanted to minimize false positive/false negative, we could also use additional metrics (ROCR) to find the optimal cut-off point. We could also employ upsampling the minority class or downsampling the majority class.

Since we care about insights, building a model is just the first step. I need to check that the model predicts well, otherwise, it's useless. Afterall, we want to understand what drives a user to convert on an impression. 


Let’s start by checking variable importance from the model:

```{r}
varImpPlot(rf,type=2)
```

###Conclusion:

1. The features: user click through rate, bid floor and user impression are among the top features driving model performance. 
2. The native layout, target type, and device type have the least impact on model performance. These are variables I'd remove going forward. 
3. I'd engineer several other features to gain more insight. In addition, I'd also be more rigorous with data processing to impute missing values. 
4. I'd start with the advertisers who are significantly exceeding specified cpa goals but also take a look at the ones who are significantly below. They could stand to benefit from optimized bidding practices.




