"""
Data processing utility function
  
"""
# Import Python libraries
from __future__ import division
from pdb import set_trace as debug
import json
import cPickle
import logging

def getLogger():
    """
    Set-up logger
    """
    # Create logger
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    ch = logging.StreamHandler()
    formatter = \
        logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    return logger


def save_pickle_model(data, model, data_args):
    """
    Save model
    """
    # Fit model
    clf = model.fit(data)
    
    # Save model 
    with open (path_names["pickle_model_file"], "wb") as model_file:
      cPickle.dump(clf, model_file)
    logger.info("Successfully saved model")
    return


def load_model():
    """
    Load model
    """

    # Load model 
    with open (path_names["pickle_model_file"], "rb") as model_file:
      clf = cPickle.load(clf, model_file)
    logger.info("Successfully loaded model")
    return





