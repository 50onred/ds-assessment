"""Evaluation utils

Usage:
  $ python evaluation_utils.py
"""
from __future__ import division
from pdb import set_trace as debug
import json
from collections import OrderedDict
import pandas as pd
import numpy as np 
import pandas as pd
import logging 
import argparse 

from sklearn.cross_validation import train_test_split, KFold
from sklearn import metrics
from classifier_models import RFClassifier, GBClassifier
from data_processing_utils import getLogger

logger = getLogger()

def train_test(data_train, data_test, model):
    model.fit(data_train)
    y_pred = model.predict(data_test)
    y_probs = model.predict_proba(data_test) 
    logger.info("Successfully fit the model and made predictions")
    
    y_test = data_test[model.get_label_col()]
    result_map = {
        "auroc": metrics.roc_auc_score(y_test, y_probs[:, 1]),
        "accuracy": metrics.accuracy_score(y_test, y_pred),
        "precision": metrics.precision_score(y_test, y_pred),
        "recall": metrics.recall_score(y_test, y_pred)
    }
    logger.info("Successfully completed train_test() function")
    return result_map


def cross_validate(data, model, kfold_n, cv_folds=3):
    #Note: older version of cross validate requires number of 
    # records in data set for KFold. Updated version of cross
    # validate doesn't require it. It's not on the cluster. 
    kf = KFold(n=kfold_n) 
    
    cv_df = pd.DataFrame()
    fold_idx = 0
    for train_index, test_index in kf:
        data_train, data_test = data.loc[train_index, :], data.loc[test_index, :]
        result = pd.DataFrame([train_test(data_train, data_test, model)])
        
        cv_df =  pd.concat([cv_df, pd.DataFrame(result)])
        logger.info("Successfully completed fold no. %d" % fold_idx)
        fold_idx += 1
    cv_df.reset_index(drop=True, inplace=True)
    
    # Calculate the average CV scores
    median_df = cv_df.median().to_frame().transpose()
    median_df["avg_type"] = "median"
    mean_df = cv_df.mean().to_frame().transpose()
    mean_df["avg_type"] = "mean"
    performance = pd.concat([mean_df, median_df])
    return performance


def split_train_validate(data, train_size=0.8, shuffle=False, seed=None):
    assert train_size <= 1 and train_size > 0
    data.reset_index(drop=True, inplace=True)
    
    if train_size == 1:  # it means all samples go to train
        val_data = None
        train_data = data.copy()
        if shuffle:  # Shuffle all rows
            train_data = train_data.sample(frac=1, random_state=seed).reset_index(drop=True)
    else:
        train_idx, val_idx = train_test_split(data.index, train_size=train_size,
                                              shuffle=shuffle, random_state=seed)
        train_data = data.loc[train_idx].reset_index(drop=True)
        val_data = data.loc[val_idx].reset_index(drop=True)

    return({"train": train_data,
            "validate": val_data})


def down_sample(data, label_col, pos_size=0.5, seed=None):
    """Down-sample the negative samples in the original imbalanced data

    :param data:
    :param label_col:
    :param pos_size: Size (percentage) of the positive samples desired
    :param seed:
    :return:
    """
    pos_data = data[data[label_col] == 1].copy()  # positive samples
    pos_length = len(pos_data)

    neg_data = data[data[label_col] == 0].copy()  # negative samples
    neg_length = (1.0 - pos_size) / pos_size * pos_length

    neg_data = neg_data.sample(n=neg_length, replace=False, random_state=seed)

    data = pd.concat([pos_data, neg_data]).sample(frac=1, replace=False).reset_index()
    return data


def up_sample_naive(data, label_col, pos_size = 0.5, seed=None):
    """Up-sample the postive samples in the original imbalanced data using naive method

    :param data:
    :param label_col:
    :param pos_size: Size (percentage) of the positive samples desired
    :param seed:
    :return:
    """
    neg_length = sum(data[label_col] == 0)

    pos_length = pos_size / (1.0 - pos_size) * neg_length
    pos_data = data[data[label_col] == 1].copy()  # positive samples
    assert pos_length >= len(pos_data), "Required positive sample length cannot be smaller than the current"

    pos_data_new = pos_data.sample(n=pos_length - len(pos_data), replace=True)
    data = pd.concat([data, pos_data_new]).sample(frac=1, replace=False).reset_index()
    return data
